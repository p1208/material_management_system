﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace material_management_sysem
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void forNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // #Logic : Directiong towards NewUser form
            NewUser nu = new NewUser();
            nu.ShowDialog();
        }

        private void toChangePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // # Logic : Directing towards password_man
            password_man pasm = new password_man();
            pasm.ShowDialog();
        }

        private void forShopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // # Logic : Directing towards configmaster
            configmaster conf = new configmaster();
            conf.ShowDialog();
        }
    }
}
