﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace material_management_sysem
{
    public partial class password_man : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Raaven"].ToString());
        public password_man()
        {
            InitializeComponent();
        }

        private void password_man_Load(object sender, EventArgs e)
        {
            
        }

        // #search button 
        // ##Logic : Displaying data inside list box
        private void txtsuname_TextChanged(object sender, EventArgs e)
        {
            if (txtsuname.Text != "")
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select username from Tblusermaster where username like '" + txtsuname.Text + "%'", con);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                lbuname.Items.Clear();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        lbuname.Visible = true;
                        lbuname.Items.Add(dr[0]);
                    }
                }
                else
                {
                    lbuname.Visible = false;
                }
            }
            else
            {
                lbuname.Visible = false;
            }
        }

        //##Logic : Taking  focus on list box
        private void txtsuname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                lbuname.Focus();
            }
        }

        // #Global_declaration
        string password;

        //##Logic :  Searching User Data
        private void lbuname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //###Logic : Inserting username from listbox to txtbox for searching
            if (e.KeyChar == 13)
            {
                txtsuname.Text = lbuname.SelectedItem.ToString();
                lbuname.Visible = false;
            }

            //###Logic : Searching and filling user data by user name
            SqlDataAdapter adapter = new SqlDataAdapter("select * from v_man_udetail where username='" + txtsuname.Text + "'", con);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                txtuid.Text = ds.Tables[0].Rows[0][0].ToString();
                txtuname.Text = ds.Tables[0].Rows[0][1].ToString();
                password = ds.Tables[0].Rows[0][2].ToString();
                txtfname.Text = ds.Tables[0].Rows[0][4].ToString();
                txtmname.Text = ds.Tables[0].Rows[0][5].ToString();
                txtlname.Text = ds.Tables[0].Rows[0][6].ToString();
                txtflname.Text = ds.Tables[0].Rows[0][7].ToString();
                txtmono.Text = ds.Tables[0].Rows[0][8].ToString();
                txteid.Text = ds.Tables[0].Rows[0][9].ToString();
                txtcdate.Text = ds.Tables[0].Rows[0][10].ToString();
                txtcby.Text = ds.Tables[0].Rows[0][11].ToString();
                txtmdate.Text = ds.Tables[0].Rows[0][12].ToString();
                txtmby.Text = ds.Tables[0].Rows[0][13].ToString();
            }
            txtopassword.Focus();
        }

        //#validation
        private Boolean valid()
        {
            if (password != txtopassword.Text)
            {
                MessageBox.Show("Invalid Old Password");
                txtopassword.Focus();
                return false;
            }
            if (txtnpassword.Text == "")
            {
                MessageBox.Show("Please Enter A New Password");
                txtnpassword.Focus();
                return false;
            }
            if (txtnpassword.Text != txtcpassword.Text)
            {
                MessageBox.Show("Invalid Confirm Password");
                txtcpassword.Focus();
                return false;
            }
            return true;
        }

        // #save_button_codeing
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                try
                {
                    // #Logic : For passing data from txtbox to the database via procedure
                    SqlCommand cmd = new SqlCommand();
                    if (txtuid.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@userid", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@userid", txtuid.Text);
                    }
                    cmd.Parameters.AddWithValue("@username", txtuname.Text);
                    cmd.Parameters.AddWithValue("@password", txtnpassword.Text);
                    cmd.Parameters.AddWithValue("@con_password", txtcpassword.Text);
                    cmd.Parameters.AddWithValue("@fname", txtfname.Text);
                    cmd.Parameters.AddWithValue("@mname", txtmname.Text);
                    cmd.Parameters.AddWithValue("@lname", txtlname.Text);
                    cmd.Parameters.AddWithValue("@mobileno", txtmono.Text);
                    cmd.Parameters.AddWithValue("@emailid", txteid.Text);
                    cmd.Parameters.AddWithValue("@created_by", Login.Username);
                    cmd.Parameters.AddWithValue("@modified_by", Login.Username);

                    // #Logic : getting error msg from database if it is occurs
                    SqlParameter ermsg = cmd.Parameters.Add("@Error_msg", SqlDbType.VarChar, 500);
                    ermsg.Direction = ParameterDirection.Output;

                    // #Logic : getting return value when procedure is executed
                    SqlParameter rvalue = cmd.Parameters.Add("@ret", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    // name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_man_udetail";
                    cmd.CommandType = CommandType.StoredProcedure;

                    // #Logic : Execution of procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int ret = 0;
                    ret = (int)rvalue.Value;

                    // ##Logic : for success
                    if (ret > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");
                        // ### Loogic : clearing data prescent in the txtbox
                        txtuname.Clear();
                        txtopassword.Clear();
                        txtnpassword.Clear();
                        txtcpassword.Clear();
                        txtfname.Clear();
                        txtmname.Clear();
                        txtlname.Clear();
                        txtmono.Clear();
                        txteid.Clear();
                        txtsuname.Clear();
                        txtuid.Clear();
                        txtcdate.Clear();
                        txtcby.Clear();
                        txtmdate.Clear();
                        txtmby.Clear();
                        txtflname.Clear();
                    }
                    else
                    {
                        // ##Logic : printing Error while executiong procedure if it occurs
                        string str = (string)ermsg.Value;
                        MessageBox.Show(str);
                    }
                }
                catch (Exception ex)
                {
                    // ##Logic : printing error while executing try  block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        // #clear_button_setting
        private void btnclear_Click(object sender, EventArgs e)
        {
            txtuname.Clear();
            txtopassword.Clear();
            txtnpassword.Clear();
            txtcpassword.Clear();
            txtfname.Clear();
            txtmname.Clear();
            txtlname.Clear();
            txtmono.Clear();
            txteid.Clear();
            txtsuname.Clear();
            txtuid.Clear();
            txtcdate.Clear();
            txtcby.Clear();
            txtmdate.Clear();
            txtmby.Clear();
            txtflname.Clear();
        }

        // #exit_button_coding
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
