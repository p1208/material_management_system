﻿
namespace material_management_sysem
{
    partial class NewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtflname = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.butn_exit = new System.Windows.Forms.Button();
            this.butn_clear = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.lbuname = new System.Windows.Forms.ListBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtmby = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcby = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtmdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcdate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtsuname = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txteid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtmono = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtlname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtcpassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtuname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtflname
            // 
            this.txtflname.Enabled = false;
            this.txtflname.Location = new System.Drawing.Point(402, 387);
            this.txtflname.Margin = new System.Windows.Forms.Padding(2);
            this.txtflname.Name = "txtflname";
            this.txtflname.Size = new System.Drawing.Size(134, 22);
            this.txtflname.TabIndex = 155;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(303, 390);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 17);
            this.label19.TabIndex = 154;
            this.label19.Text = "Full Name";
            // 
            // txtuid
            // 
            this.txtuid.Enabled = false;
            this.txtuid.Location = new System.Drawing.Point(404, 209);
            this.txtuid.Margin = new System.Windows.Forms.Padding(2);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(134, 22);
            this.txtuid.TabIndex = 153;
            this.txtuid.TextChanged += new System.EventHandler(this.txtuid_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(305, 212);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 17);
            this.label18.TabIndex = 152;
            this.label18.Text = "User ID";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(387, 5);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(151, 22);
            this.dateTimePicker1.TabIndex = 151;
            // 
            // butn_exit
            // 
            this.butn_exit.Location = new System.Drawing.Point(378, 434);
            this.butn_exit.Margin = new System.Windows.Forms.Padding(2);
            this.butn_exit.Name = "butn_exit";
            this.butn_exit.Size = new System.Drawing.Size(56, 29);
            this.butn_exit.TabIndex = 150;
            this.butn_exit.Text = "Exit";
            this.butn_exit.UseVisualStyleBackColor = true;
            this.butn_exit.Click += new System.EventHandler(this.butn_exit_Click);
            // 
            // butn_clear
            // 
            this.butn_clear.Location = new System.Drawing.Point(307, 434);
            this.butn_clear.Margin = new System.Windows.Forms.Padding(2);
            this.butn_clear.Name = "butn_clear";
            this.butn_clear.Size = new System.Drawing.Size(56, 29);
            this.butn_clear.TabIndex = 149;
            this.butn_clear.Text = "Clear";
            this.butn_clear.UseVisualStyleBackColor = true;
            this.butn_clear.Click += new System.EventHandler(this.butn_clear_Click);
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(22, 434);
            this.but_save.Margin = new System.Windows.Forms.Padding(2);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(56, 29);
            this.but_save.TabIndex = 148;
            this.but_save.Text = "Save";
            this.but_save.UseVisualStyleBackColor = true;
            this.but_save.Click += new System.EventHandler(this.but_save_Click);
            // 
            // lbuname
            // 
            this.lbuname.FormattingEnabled = true;
            this.lbuname.ItemHeight = 16;
            this.lbuname.Location = new System.Drawing.Point(402, 118);
            this.lbuname.Margin = new System.Windows.Forms.Padding(2);
            this.lbuname.Name = "lbuname";
            this.lbuname.Size = new System.Drawing.Size(134, 36);
            this.lbuname.TabIndex = 147;
            this.lbuname.Visible = false;
            this.lbuname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lbuname_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(287, 176);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 17);
            this.label17.TabIndex = 146;
            this.label17.Text = "For Technical Information";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(287, 78);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 17);
            this.label16.TabIndex = 145;
            this.label16.Text = "For Searching";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 78);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(177, 17);
            this.label15.TabIndex = 144;
            this.label15.Text = "For Insertion or Updatation";
            // 
            // txtmby
            // 
            this.txtmby.Enabled = false;
            this.txtmby.Location = new System.Drawing.Point(402, 354);
            this.txtmby.Margin = new System.Windows.Forms.Padding(2);
            this.txtmby.Name = "txtmby";
            this.txtmby.Size = new System.Drawing.Size(134, 22);
            this.txtmby.TabIndex = 143;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(303, 357);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 17);
            this.label14.TabIndex = 142;
            this.label14.Text = "Modified By";
            // 
            // txtcby
            // 
            this.txtcby.Enabled = false;
            this.txtcby.Location = new System.Drawing.Point(402, 279);
            this.txtcby.Margin = new System.Windows.Forms.Padding(2);
            this.txtcby.Name = "txtcby";
            this.txtcby.Size = new System.Drawing.Size(134, 22);
            this.txtcby.TabIndex = 141;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(303, 284);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 17);
            this.label13.TabIndex = 140;
            this.label13.Text = "Created By";
            // 
            // txtmdate
            // 
            this.txtmdate.Enabled = false;
            this.txtmdate.Location = new System.Drawing.Point(402, 320);
            this.txtmdate.Margin = new System.Windows.Forms.Padding(2);
            this.txtmdate.Name = "txtmdate";
            this.txtmdate.Size = new System.Drawing.Size(134, 22);
            this.txtmdate.TabIndex = 139;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(303, 323);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 17);
            this.label12.TabIndex = 138;
            this.label12.Text = "Modified Date";
            // 
            // txtcdate
            // 
            this.txtcdate.Enabled = false;
            this.txtcdate.Location = new System.Drawing.Point(402, 244);
            this.txtcdate.Margin = new System.Windows.Forms.Padding(2);
            this.txtcdate.Name = "txtcdate";
            this.txtcdate.Size = new System.Drawing.Size(134, 22);
            this.txtcdate.TabIndex = 137;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(303, 247);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 136;
            this.label11.Text = "Created Date";
            // 
            // txtsuname
            // 
            this.txtsuname.Location = new System.Drawing.Point(402, 102);
            this.txtsuname.Margin = new System.Windows.Forms.Padding(2);
            this.txtsuname.Name = "txtsuname";
            this.txtsuname.Size = new System.Drawing.Size(134, 22);
            this.txtsuname.TabIndex = 135;
            this.txtsuname.TextChanged += new System.EventHandler(this.txtsuname_TextChanged);
            this.txtsuname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsuname_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(303, 102);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 17);
            this.label10.TabIndex = 134;
            this.label10.Text = "Username";
            // 
            // txteid
            // 
            this.txteid.Location = new System.Drawing.Point(144, 361);
            this.txteid.Margin = new System.Windows.Forms.Padding(2);
            this.txteid.Name = "txteid";
            this.txteid.Size = new System.Drawing.Size(128, 22);
            this.txteid.TabIndex = 133;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 361);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 132;
            this.label8.Text = "Email ID";
            // 
            // txtmono
            // 
            this.txtmono.Location = new System.Drawing.Point(144, 322);
            this.txtmono.Margin = new System.Windows.Forms.Padding(2);
            this.txtmono.Name = "txtmono";
            this.txtmono.Size = new System.Drawing.Size(128, 22);
            this.txtmono.TabIndex = 131;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 322);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 17);
            this.label9.TabIndex = 130;
            this.label9.Text = "Mobile NO.";
            // 
            // txtlname
            // 
            this.txtlname.Location = new System.Drawing.Point(144, 286);
            this.txtlname.Margin = new System.Windows.Forms.Padding(2);
            this.txtlname.Name = "txtlname";
            this.txtlname.Size = new System.Drawing.Size(128, 22);
            this.txtlname.TabIndex = 129;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 286);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 128;
            this.label5.Text = "Last Name";
            // 
            // txtmname
            // 
            this.txtmname.Location = new System.Drawing.Point(144, 247);
            this.txtmname.Margin = new System.Windows.Forms.Padding(2);
            this.txtmname.Name = "txtmname";
            this.txtmname.Size = new System.Drawing.Size(128, 22);
            this.txtmname.TabIndex = 127;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 247);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 126;
            this.label6.Text = "Middle Name";
            // 
            // txtfname
            // 
            this.txtfname.Location = new System.Drawing.Point(144, 212);
            this.txtfname.Margin = new System.Windows.Forms.Padding(2);
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(128, 22);
            this.txtfname.TabIndex = 125;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 212);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 124;
            this.label7.Text = "First Name";
            // 
            // txtcpassword
            // 
            this.txtcpassword.Font = new System.Drawing.Font("Wingdings", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtcpassword.Location = new System.Drawing.Point(144, 176);
            this.txtcpassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtcpassword.Name = "txtcpassword";
            this.txtcpassword.PasswordChar = 'l';
            this.txtcpassword.Size = new System.Drawing.Size(128, 22);
            this.txtcpassword.TabIndex = 123;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 122;
            this.label4.Text = "Confirm Password";
            // 
            // txtpassword
            // 
            this.txtpassword.Font = new System.Drawing.Font("Wingdings", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtpassword.Location = new System.Drawing.Point(144, 137);
            this.txtpassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = 'l';
            this.txtpassword.Size = new System.Drawing.Size(128, 22);
            this.txtpassword.TabIndex = 121;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 137);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 120;
            this.label3.Text = "Password";
            // 
            // txtuname
            // 
            this.txtuname.Location = new System.Drawing.Point(144, 102);
            this.txtuname.Margin = new System.Windows.Forms.Padding(2);
            this.txtuname.Name = "txtuname";
            this.txtuname.Size = new System.Drawing.Size(128, 22);
            this.txtuname.TabIndex = 119;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 118;
            this.label2.Text = "Username";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(538, 32);
            this.label1.TabIndex = 117;
            this.label1.Text = "Welcome To Usermanipulation System";
            // 
            // NewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 472);
            this.Controls.Add(this.txtflname);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtuid);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.butn_exit);
            this.Controls.Add(this.butn_clear);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.lbuname);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtmby);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtcby);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtmdate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtcdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtsuname);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txteid);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtmono);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtlname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtmname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtfname);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtcpassword);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtuname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewUser";
            this.Text = "NewUser";
            this.Load += new System.EventHandler(this.NewUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtflname;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button butn_exit;
        private System.Windows.Forms.Button butn_clear;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.ListBox lbuname;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtmby;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcby;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtmdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtsuname;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txteid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtmono;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtlname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtcpassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtuname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}