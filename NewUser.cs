﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace material_management_sysem
{
    public partial class NewUser : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Raaven"].ToString());
        public NewUser()
        {
            InitializeComponent();
        }

        private void NewUser_Load(object sender, EventArgs e)
        {

        }

        // #Validation 
        private Boolean valid()
        {
            if (txtuname.Text == "")
            {
                MessageBox.Show("Please Enter Username");
                txtuname.Focus();
                return false;
            }
            if (txtpassword.Text == "")
            {
                MessageBox.Show("Please Enter The Password");
                txtpassword.Focus();
                return false;
            }
            if (txtcpassword.Text == "")
            {
                MessageBox.Show("Please Enter Confirm Password");
                txtcpassword.Focus();
                return false;
            }
            if (txtpassword.Text != txtcpassword.Text)
            {
                MessageBox.Show("Envalid Confirm Password");
                txtcpassword.Focus();
                return false;
            }
            if (txtmono.Text == "")
            {
                MessageBox.Show("Please Enter Valid Mobile Number");
                txtmono.Focus();
                return false;
            }
            if (txteid.Text == "")
            {
                MessageBox.Show("Please Enter Valid Email ID");
                txteid.Focus();
                return false;
            }
            return true;
        }

        // #saving_Button_coding
        private void but_save_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                try
                {
                    // #Logic : For passing data from txtbox to the database via procedure
                    SqlCommand cmd = new SqlCommand();
                    if (txtuid.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@userid", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@userid", txtuid.Text);
                    }
                    cmd.Parameters.AddWithValue("@username", txtuname.Text);
                    cmd.Parameters.AddWithValue("@password", txtpassword.Text);
                    cmd.Parameters.AddWithValue("@con_password", txtcpassword.Text);
                    cmd.Parameters.AddWithValue("@fname", txtfname.Text);
                    cmd.Parameters.AddWithValue("@mname", txtmname.Text);
                    cmd.Parameters.AddWithValue("@lname", txtlname.Text);
                    cmd.Parameters.AddWithValue("@mobileno", txtmono.Text);
                    cmd.Parameters.AddWithValue("@emailid", txteid.Text);
                    cmd.Parameters.AddWithValue("@created_by", Login.Username);
                    cmd.Parameters.AddWithValue("@modified_by", Login.Username);

                    // #Logic : getting error msg from database if it is occurs
                    SqlParameter ermsg = cmd.Parameters.Add("@Error_msg", SqlDbType.VarChar, 500);
                    ermsg.Direction = ParameterDirection.Output;

                    // #Logic : getting return value when procedure is executed
                    SqlParameter rvalue = cmd.Parameters.Add("@ret", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    // name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_man_udetail";
                    cmd.CommandType = CommandType.StoredProcedure;

                    // #Logic : Execution of procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int ret = 0;
                    ret = (int)rvalue.Value;
                    
                    // ##Logic : for success
                    if (ret > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");
                        // ### Loogic : clearing data prescent in the txtbox
                        txtuname.Clear();
                        txtpassword.Clear();
                        txtcpassword.Clear();
                        txtfname.Clear();
                        txtmname.Clear();
                        txtlname.Clear();
                        txtmono.Clear();
                        txteid.Clear();
                        txtsuname.Clear();
                        txtuid.Clear();
                        txtcdate.Clear();
                        txtcby.Clear();
                        txtmdate.Clear();
                        txtmby.Clear();
                        txtflname.Clear();
                    }
                    else
                    {
                        // ##Logic : printing Error while executiong procedure if it occurs
                        string str = (string)ermsg.Value;
                        MessageBox.Show(str);
                    }
                }
                catch (Exception ex)
                {
                    // ##Logic : printing error while executing try  block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        // #closing_current_form 
        private void butn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // #clearing_data_prescent_in_the_txtbox
        private void butn_clear_Click(object sender, EventArgs e)
        {
            txtuname.Clear();
            txtpassword.Clear();
            txtcpassword.Clear();
            txtfname.Clear();
            txtmname.Clear();
            txtlname.Clear();
            txtmono.Clear();
            txteid.Clear();
            txtsuname.Clear();
            txtuid.Clear();
            txtcdate.Clear();
            txtcby.Clear();
            txtmdate.Clear();
            txtmby.Clear();
            txtflname.Clear();
        }

        // #search button 
        // ##Logic : Displaying data inside list box
        private void txtsuname_TextChanged(object sender, EventArgs e)
        {
            if (txtsuname.Text != "")
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select username from Tblusermaster where username like '" + txtsuname.Text + "%'", con);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                lbuname.Items.Clear();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        lbuname.Visible = true;
                        lbuname.Items.Add(dr[0]);
                    }
                }
                else
                {
                    lbuname.Visible = false;
                }
            }
            else
            {
                lbuname.Visible = false;
            }
        }

        //##Logic : Taking  focus on list box
        private void txtsuname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                lbuname.Focus();
            }
        }

        //##Logic :  Searching User Data
        private void lbuname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //###Logic : Inserting username from listbox to txtbox for searching
            if (e.KeyChar == 13)
            {
                txtsuname.Text = lbuname.SelectedItem.ToString();
                lbuname.Visible = false;
            }

            //###Logic : Searching and filling user data by user name
            SqlDataAdapter adapter = new SqlDataAdapter("select * from v_man_udetail where username='" + txtsuname.Text + "'", con);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                txtuid.Text = ds.Tables[0].Rows[0][0].ToString();
                txtuname.Text = ds.Tables[0].Rows[0][1].ToString();
                txtpassword.Text = ds.Tables[0].Rows[0][2].ToString();
                txtcpassword.Text = ds.Tables[0].Rows[0][3].ToString();
                txtfname.Text = ds.Tables[0].Rows[0][4].ToString();
                txtmname.Text = ds.Tables[0].Rows[0][5].ToString();
                txtlname.Text = ds.Tables[0].Rows[0][6].ToString();
                txtflname.Text = ds.Tables[0].Rows[0][7].ToString();
                txtmono.Text = ds.Tables[0].Rows[0][8].ToString();
                txteid.Text = ds.Tables[0].Rows[0][9].ToString();
                txtcdate.Text = ds.Tables[0].Rows[0][10].ToString();
                txtcby.Text = ds.Tables[0].Rows[0][11].ToString();
                txtmdate.Text = ds.Tables[0].Rows[0][12].ToString();
                txtmby.Text = ds.Tables[0].Rows[0][13].ToString();
            }
        }

        //##Logic : Putting restrictions on updating password and confirm password while updation
        private void txtuid_TextChanged(object sender, EventArgs e)
        {
            if (txtuid.Text != "")
            {
                txtpassword.Enabled = false;
                txtcpassword.Enabled = false;
            }
            else
            {
                txtpassword.Enabled = true;
                txtcpassword.Enabled = true;
            }
        }
    }
}
