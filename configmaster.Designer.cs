﻿
namespace material_management_sysem
{
    partial class configmaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtmby = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcby = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtmdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcdate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbsname = new System.Windows.Forms.ListBox();
            this.txtssname = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtadress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtcono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbmono = new System.Windows.Forms.CheckBox();
            this.txtmono = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtpropwriter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txteid = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(480, 5);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(183, 22);
            this.dateTimePicker1.TabIndex = 161;
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(436, 522);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(75, 36);
            this.btnexit.TabIndex = 160;
            this.btnexit.Text = "Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(342, 522);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(75, 36);
            this.btnclear.TabIndex = 159;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(48, 522);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 36);
            this.btnsave.TabIndex = 158;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // txtcid
            // 
            this.txtcid.Enabled = false;
            this.txtcid.Location = new System.Drawing.Point(449, 242);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(178, 22);
            this.txtcid.TabIndex = 157;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(348, 242);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 17);
            this.label18.TabIndex = 156;
            this.label18.Text = "Config ID";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(324, 198);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 17);
            this.label17.TabIndex = 155;
            this.label17.Text = "For Technical Information";
            // 
            // txtmby
            // 
            this.txtmby.Enabled = false;
            this.txtmby.Location = new System.Drawing.Point(446, 421);
            this.txtmby.Name = "txtmby";
            this.txtmby.Size = new System.Drawing.Size(178, 22);
            this.txtmby.TabIndex = 154;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(345, 421);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 17);
            this.label14.TabIndex = 153;
            this.label14.Text = "Modified By";
            // 
            // txtcby
            // 
            this.txtcby.Enabled = false;
            this.txtcby.Location = new System.Drawing.Point(446, 328);
            this.txtcby.Name = "txtcby";
            this.txtcby.Size = new System.Drawing.Size(178, 22);
            this.txtcby.TabIndex = 152;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(345, 331);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 17);
            this.label13.TabIndex = 151;
            this.label13.Text = "Created By";
            // 
            // txtmdate
            // 
            this.txtmdate.Enabled = false;
            this.txtmdate.Location = new System.Drawing.Point(446, 379);
            this.txtmdate.Name = "txtmdate";
            this.txtmdate.Size = new System.Drawing.Size(178, 22);
            this.txtmdate.TabIndex = 150;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(345, 379);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 17);
            this.label12.TabIndex = 149;
            this.label12.Text = "Modified Date";
            // 
            // txtcdate
            // 
            this.txtcdate.Enabled = false;
            this.txtcdate.Location = new System.Drawing.Point(446, 285);
            this.txtcdate.Name = "txtcdate";
            this.txtcdate.Size = new System.Drawing.Size(178, 22);
            this.txtcdate.TabIndex = 148;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(345, 285);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 147;
            this.label11.Text = "Created Date";
            // 
            // lbsname
            // 
            this.lbsname.FormattingEnabled = true;
            this.lbsname.ItemHeight = 16;
            this.lbsname.Location = new System.Drawing.Point(449, 143);
            this.lbsname.Name = "lbsname";
            this.lbsname.Size = new System.Drawing.Size(175, 52);
            this.lbsname.TabIndex = 146;
            this.lbsname.Visible = false;
            this.lbsname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lbsname_KeyPress);
            // 
            // txtssname
            // 
            this.txtssname.Location = new System.Drawing.Point(449, 125);
            this.txtssname.Name = "txtssname";
            this.txtssname.Size = new System.Drawing.Size(175, 22);
            this.txtssname.TabIndex = 145;
            this.txtssname.TextChanged += new System.EventHandler(this.txtssname_TextChanged);
            this.txtssname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtssname_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(349, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 17);
            this.label9.TabIndex = 144;
            this.label9.Text = "Shopname";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(324, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 17);
            this.label8.TabIndex = 143;
            this.label8.Text = "For Searching ";
            // 
            // txtadress
            // 
            this.txtadress.Location = new System.Drawing.Point(142, 384);
            this.txtadress.Multiline = true;
            this.txtadress.Name = "txtadress";
            this.txtadress.Size = new System.Drawing.Size(165, 98);
            this.txtadress.TabIndex = 142;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 384);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 17);
            this.label7.TabIndex = 141;
            this.label7.Text = "Adress";
            // 
            // txtcono
            // 
            this.txtcono.Location = new System.Drawing.Point(142, 299);
            this.txtcono.Name = "txtcono";
            this.txtcono.Size = new System.Drawing.Size(165, 22);
            this.txtcono.TabIndex = 140;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 139;
            this.label6.Text = "Contact No.";
            // 
            // cbmono
            // 
            this.cbmono.AutoSize = true;
            this.cbmono.Location = new System.Drawing.Point(51, 235);
            this.cbmono.Name = "cbmono";
            this.cbmono.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbmono.Size = new System.Drawing.Size(112, 55);
            this.cbmono.TabIndex = 138;
            this.cbmono.Text = "If Mobile No\r\nAre Same As\r\nContact No";
            this.cbmono.UseVisualStyleBackColor = true;
            this.cbmono.CheckedChanged += new System.EventHandler(this.cbmono_CheckedChanged);
            // 
            // txtmono
            // 
            this.txtmono.Location = new System.Drawing.Point(142, 207);
            this.txtmono.Name = "txtmono";
            this.txtmono.Size = new System.Drawing.Size(165, 22);
            this.txtmono.TabIndex = 137;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 136;
            this.label5.Text = "Mobile No.";
            // 
            // txtpropwriter
            // 
            this.txtpropwriter.Location = new System.Drawing.Point(142, 166);
            this.txtpropwriter.Name = "txtpropwriter";
            this.txtpropwriter.Size = new System.Drawing.Size(165, 22);
            this.txtpropwriter.TabIndex = 135;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 134;
            this.label4.Text = "Propwriter";
            // 
            // txtsname
            // 
            this.txtsname.Location = new System.Drawing.Point(142, 128);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(165, 22);
            this.txtsname.TabIndex = 133;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 132;
            this.label3.Text = "Shopname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 17);
            this.label2.TabIndex = 131;
            this.label2.Text = "For Insertion And Update";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(650, 32);
            this.label1.TabIndex = 130;
            this.label1.Text = "Welcome To Configration Manipulation System";
            // 
            // txteid
            // 
            this.txteid.Location = new System.Drawing.Point(142, 346);
            this.txteid.Name = "txteid";
            this.txteid.Size = new System.Drawing.Size(165, 22);
            this.txteid.TabIndex = 163;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 346);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 17);
            this.label10.TabIndex = 162;
            this.label10.Text = "Email_ID";
            // 
            // configmaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 570);
            this.Controls.Add(this.txteid);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.txtcid);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtmby);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtcby);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtmdate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtcdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lbsname);
            this.Controls.Add(this.txtssname);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtadress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtcono);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbmono);
            this.Controls.Add(this.txtmono);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtpropwriter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtsname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "configmaster";
            this.Text = "configmaster";
            this.Load += new System.EventHandler(this.configmaster_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtmby;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcby;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtmdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox lbsname;
        private System.Windows.Forms.TextBox txtssname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtadress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtcono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbmono;
        private System.Windows.Forms.TextBox txtmono;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtpropwriter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txteid;
        private System.Windows.Forms.Label label10;
    }
}