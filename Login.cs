﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace material_management_sysem
{
    public partial class Login : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Raaven"].ToString());
        public Login()
        {
            InitializeComponent();
        }

        // #validation 
        private Boolean valid()
        {
            if (txtuname.Text == "")
            {
                MessageBox.Show("Please Enter Username");
                txtuname.Focus();
                return false;
            }
            if (txtpassword.Text == "")
            {
                MessageBox.Show("Please Enter Password");
                txtpassword.Focus();
                return false;
            }
            return true;
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        // #Global_declaration
        string password;
        public static string Username;
        // #Login_button_coding
        private void btn_login_Click(object sender, EventArgs e)
        {
            // ##Logic : Fetching username and password from database
            SqlDataAdapter adpter = new SqlDataAdapter("select username,convert(nvarchar(20),DECRYPTBYPASSPHRASE('Raaven',password))as password from Tblusermaster where username='" + txtuname.Text + "'", con);
            DataSet ds = new DataSet();
            adpter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Username = ds.Tables[0].Rows[0][0].ToString();
                password = ds.Tables[0].Rows[0][1].ToString();
                if (txtpassword.Text == password)
                {
                    // ##Logic : directing towards menu form if password is correct
                    menu m = new menu();
                    m.Show();
                    Login lin = new Login();
                    lin.Hide();
                    txtuname.Clear();
                    txtpassword.Clear();
                }
                else
                {
                    MessageBox.Show("Invalid Password");
                }
            }
            else
            {
                MessageBox.Show("Invalid Username");
            }
        }
    }
}
